'use strict';
var $ = require('../../node_modules/jquery');


$(function() {

  // Start of TAB

  $("document").ready(function(){
    $(".tab-slider--body").hide();
    $(".tab-slider--body:first").show();
  });

  $(".tab-slider--nav li").click(function() {
    $(".tab-slider--body").hide();
    var activeTab = $(this).attr("rel");
    $("#"+activeTab).fadeIn();
      if($(this).attr("rel") == "tab2"){
        $('.tab-slider--tabs').addClass('slide');
        $('.section-title--slider').text("Web Development");
      } else if($(this).attr("rel") == "tab1") {
        $('.card').removeClass('u-fade-up');
        $('.card').removeClass('u-fade-up-active');
        $('.section-title--slider').text("All Works");
      }else{
        $('.tab-slider--tabs').removeClass('slide');
        $('.section-title--slider').text("Web Application");
      }
    $(".tab-slider--nav li").removeClass("active");
    $(this).addClass("active");
  });

  // End of TAB

  // Start of Card Animation

  function fade(a) {
    $("." + a).each(function(){ 
      var b = a + "-active";
      var $window = $(window);
      var viewport_top = $window.scrollTop();
      var viewport_height = $window.height();
      var viewport_bottom = viewport_top + viewport_height;
      var $elem = $(this);
      var top = $elem.offset().top;
      var height = $elem.height();
      var bottom = top + height;
      if((top >= viewport_top && top < viewport_bottom) ||
                 (bottom > viewport_top && bottom <= viewport_bottom) ||
                 (height > viewport_height && top <= viewport_top && bottom >= viewport_bottom)) {
        $(this).addClass(b);
      }
    });
  }

  $(window).on("load scroll", function(){
    fade("u-fade-up");
  });

  // End of Card Animation

  // Start of Scroll Top

  var returnTopBtn = $('.scrollTop'),
    btnView = 200,
    speed = 400;
    returnTopBtn.hide();

    $(window).scroll(function () {
        if ($(this).scrollTop() > btnView) {
          returnTopBtn.fadeIn();
        } else {
          returnTopBtn.fadeOut();
        }
    });

    returnTopBtn.click(function () {
        var speed = 500,
        href= $(this).attr('href'),
        target = $(href == '#' || href == '' ? 'html' : href),
        position = target.offset().top;
        $('html, body').animate({
          scrollTop: position
        }, speed, 'swing');
        return false;
    });


});
